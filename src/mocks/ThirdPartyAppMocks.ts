import { Ticket } from "@models/Ticket";


export const ThirdPartyApp1Mock = {
  createTicket(problemId: string, count: number): Ticket {
    return {
      problemId,
      status: 'running',
      count,
      thirdParty: 'ThirdPartyApp1',
    };
  },
  updateTicket(problemId: string, status: 'running' | 'ongoing' | 'resolved' | 'ended', count: number): Ticket {
    return {
      problemId,
      status,
      count,
      thirdParty: 'ThirdPartyApp1',
    };
  },
  closeTicket(problemId: string, count: number): Ticket {
    return {
      problemId,
      status: 'resolved',
      count,
      thirdParty: 'ThirdPartyApp1',
    };
  },
};



export const ThirdPartyApp2Mock = {
    createTicket(ProblemRef: string, IssuesCount: number) {
      return {
        ProblemRef,
        Status: 'ongoing',
        IssuesCount,
        ExternalOwner: 'csTeam'
      };
    },
    updateTicket(ProblemRef: string, IssuesCount: number) {
      return {
        ProblemRef,
        Status: 'ongoing',
        IssuesCount,
        ExternalOwner: 'csTeam'
      };
    },
    closeTicket(ProblemRef: string, IssuesCount: number) {
      return {
        ProblemRef,
        Status: 'ended',
        IssuesCount,
        ExternalOwner: 'csTeam'
      };
    }
  };
