import { Issue } from '@models/Issue';

export class IssueService {
  static groupIssues(issues: Issue[]): Map<string, Issue[]> {
    const grouped = new Map<string, Issue[]>();

    issues.forEach(issue => {
      const key = `${issue.video}-${issue.category}`;
      if (grouped.has(key)) {
        grouped.get(key)?.push(issue);
      } else {
        grouped.set(key, [issue]);
      }
    });

    return grouped;
  }
}
