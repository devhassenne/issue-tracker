import { Ticket } from '@models/Ticket';
import { ThirdPartyApp1Mock, ThirdPartyApp2Mock } from '../mocks/ThirdPartyAppMocks';

export class ThirdPartyService {
  static createTicket(problemId: string, thirdParty: string, count: number): Ticket {
    if (thirdParty === 'ThirdPartyApp1') {
      return ThirdPartyApp1Mock.createTicket(problemId, count);
    } else if (thirdParty === 'ThirdPartyApp2') {
      const { ProblemRef, Status, IssuesCount, ExternalOwner } = ThirdPartyApp2Mock.createTicket(problemId, count);
      return {
        problemId: ProblemRef,
        status: Status as Ticket['status'],
        count: IssuesCount,
        thirdParty: thirdParty,
      };
    }
    throw new Error("Invalid third-party service");
  }

  static updateTicket(problemId: string, thirdParty: string, count: number): Ticket {
    if (thirdParty === 'ThirdPartyApp1') {
      return ThirdPartyApp1Mock.updateTicket(problemId, 'ongoing', count);
    } else if (thirdParty === 'ThirdPartyApp2') {
      const { ProblemRef, IssuesCount } = ThirdPartyApp2Mock.updateTicket(problemId, count);
      return {
        problemId: ProblemRef,
        status: 'ongoing',
        count: IssuesCount,
        thirdParty: thirdParty,
      };
    }
    throw new Error("Invalid third-party service");
  }

  static closeTicket(problemId: string, thirdParty: string, count: number): Ticket {
    if (thirdParty === 'ThirdPartyApp1') {
      return ThirdPartyApp1Mock.closeTicket(problemId, count);
    } else if (thirdParty === 'ThirdPartyApp2') {
      const { ProblemRef, IssuesCount } = ThirdPartyApp2Mock.closeTicket(problemId, count);
      return {
        problemId: ProblemRef,
        status: 'resolved',
        count: IssuesCount,
        thirdParty: thirdParty,
      };
    }
    throw new Error("Invalid third-party service");
  }
}
