import { Problem } from '@models/Problem';
import {Issue} from "@models/Issue";

export class ProblemService {
  static problems: Problem[] = []; // Simulated database


  static createProblem(issues: Issue[]): Problem {
    const problem: Problem = {
      id: `prob-${Math.random().toString(36).substr(2, 9)}`, // Simple unique ID generation
      issues,
      status: 'pending', // Default status
    };
    this.problems.push(problem);
    return problem;
  }
  static updateProblemStatus(problemId: string, newStatus: "pending" | "ready" | "open" | "closed"): Problem | undefined {
    const problem = this.problems.find(p => p.id === problemId);
    if (problem && ['pending', 'ready', 'open', 'closed'].includes(newStatus)) {
      problem.status = newStatus;
    }
    return problem;
  }

  static getProblemById(problemId: string) {
    return this.problems.find(problem => problem.id === problemId);
  }
}
