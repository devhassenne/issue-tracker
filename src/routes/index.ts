import express from 'express';
import { IssueController, ProblemController } from '../controllers';

const router = express.Router();

router.post('/issues', IssueController.createIssues);
router.post('/change-status/:status', ProblemController.changeStatus);
router.post('/create-ticket/:thirdParty/:problemId', ProblemController.createTicket);
router.post('/update-ticket/:thirdParty/:problemId', ProblemController.updateTicket);
router.post('/close-ticket/:thirdParty/:problemId', ProblemController.closeTicket);

export default router;
