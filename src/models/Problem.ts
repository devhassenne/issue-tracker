import { Issue } from './Issue';

export interface Problem {
  id: string;
  issues: Issue[];
  status: 'pending' | 'ready' | 'open' | 'closed';
}
