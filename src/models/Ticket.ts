export interface Ticket {
    problemId: string;
    status: 'running' | 'resolved' | 'ongoing' | 'ended';
    count: number;
    thirdParty: 'ThirdPartyApp1' | 'ThirdPartyApp2';
  }
  