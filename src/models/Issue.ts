export interface Issue {
    video: string;
    category: string;
    userId: number;
    comment: string;
    status: 'waiting' | 'grouped';
  }
  