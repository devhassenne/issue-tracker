import { Request, Response } from 'express';
import { IssueService, ProblemService } from '../services';

export class IssueController {
  static createIssues(req: Request, res: Response) {
    const issues = req.body;
    const groupedIssues = IssueService.groupIssues(issues);
    const problems = [];

    groupedIssues.forEach((issues, key) => {
      const problem = ProblemService.createProblem(Array.from(issues));
      problems.push(problem);
    });

    res.status(201).json({ problems });
  }
}
