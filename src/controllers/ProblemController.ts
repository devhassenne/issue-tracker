import { Request, Response } from 'express';
import {IssueService, ProblemService, ThirdPartyService} from '../services';
import {Issue} from "@models/Issue";

export class ProblemController {
	static changeStatus(req: Request, res: Response) {
    const status = req.params.status as "pending" | "ready" | "open" | "closed";
    const { problemId } = req.body;

    if (!['pending', 'ready', 'open', 'closed'].includes(status)) {
      return res.status(400).json({ message: 'Invalid status value' });
    }

    const updatedProblem = ProblemService.updateProblemStatus(problemId, status);
    if (!updatedProblem) {
      return res.status(404).json({ message: 'Problem not found' });
    }

    res.status(200).json({ updatedProblem });
  }

  static createTicket(req: Request, res: Response) {
    const { thirdParty, problemId } = req.params;
    const problem = ProblemService.getProblemById(problemId);
    if (!problem || problem.status !== 'ready') {
      return res.status(400).json({ message: 'Problem not ready or not found' });
    }

    const ticket = ThirdPartyService.createTicket(problemId, thirdParty, problem.issues.length);
    res.status(201).json({ ticket });
  }

  static updateTicket(req: Request, res: Response) {
    const { thirdParty, problemId } = req.params;
    const problem = ProblemService.getProblemById(problemId);

    if (!problem) {
      return res.status(404).json({ message: 'Problem not found' });
    }

    if (problem.status !== 'open') {
      return res.status(400).json({ message: 'Problem must be in "open" status to update ticket' });
    }

    const ticket = ThirdPartyService.updateTicket(problemId, thirdParty, problem.issues.length);
    res.status(200).json({ ticket });
  }


  static closeTicket(req: Request, res: Response) {
    const { thirdParty, problemId } = req.params;
    const problem = ProblemService.getProblemById(problemId);

    if (!problem) {
      return res.status(404).json({ message: 'Problem not found' });
    }

    if (problem.status !== 'open') {
      return res.status(400).json({ message: 'Problem must be in "open" status to close ticket' });
    }

    // Close the ticket
    const ticket = ThirdPartyService.closeTicket(problemId, thirdParty, problem.issues.length);

    // Optionally, update the problem status to closed if that is part of your application logic
    problem.status = 'closed';

    res.status(200).json({ ticket });
  }


  static createIssues(req: Request, res: Response) {
    const issues = req.body;
    let groupedIssues: Map<string, Issue[]>;
    groupedIssues = IssueService.groupIssues(issues);
    const problems = [];

    groupedIssues.forEach((issues, key) => {
      const problem = ProblemService.createProblem(Array.from(issues));
      problems.push(problem);
    });

    res.status(201).json({ problems });
  }

}
