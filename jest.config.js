module.exports = {
	preset: 'ts-jest',
	testEnvironment: 'node',
	transform: {
		'^.+\\.ts?$': 'ts-jest',
	},
	verbose: true,
	collectCoverage: false,
	transformIgnorePatterns: ['<rootDir>/node_modules/'],
	coverageReporters: [
		'json',
		'html',
		'lcov',
		'cobertura',
		'text-summary',
	],
	watchman: false,
}
