# Issue Tracker

This project is an issue tracking system designed to help teams manage issues and tickets effectively. It integrates with third-party services for ticket creation and management, offering a seamless experience for tracking the status of various issues.

## Project Architecture

<p align="center">
  <img src="images/docArch.png" alt="Project Architecture Graph" />
</p>

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


## Prerequisites

What things you need to install the software and how to install them:

```
Node.js (https://nodejs.org/)
Git (https://git-scm.com/downloads)
```

# Installation


A step-by-step series of examples that tell you how to get a development environment running:

## Clone the repository

```
git clone https://gitlab.com/devhassenne/issue-tracker.git
cd issue-tracker
```

## Install dependencies
```
npm install
```

## Usage
Run this command Line:
```
npm start
```
and for Test:

```
npm test
```

## Stay in touch

- Author - HBY

## License
For open source projects, say how it is licensed.

