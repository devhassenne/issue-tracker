import { IssueService } from '../../src/services';
import { Issue } from '@models/Issue';

const mockIssues: Issue[] = [
  { video: 'Video1', category: 'Category1', userId: 1, comment: 'Issue 1', status: 'waiting' },
  { video: 'Video1', category: 'Category1', userId: 2, comment: 'Issue 2', status: 'waiting' },
  { video: 'Video2', category: 'Category2', userId: 3, comment: 'Issue 3', status: 'waiting' },
];

describe('IssueService', () => {
  describe('groupIssues', () => {
    it('should group issues by video and category', () => {
      const grouped = IssueService.groupIssues(mockIssues);

      expect(grouped.size).toBe(2);
      expect(grouped.get('Video1-Category1')?.length).toBe(2);
      expect(grouped.get('Video2-Category2')?.length).toBe(1);
      expect(grouped.get('Video1-Category1')).toEqual([
        { video: 'Video1', category: 'Category1', userId: 1, comment: 'Issue 1', status: 'waiting' },
        { video: 'Video1', category: 'Category1', userId: 2, comment: 'Issue 2', status: 'waiting' },
      ]);
      expect(grouped.get('Video2-Category2')).toEqual([
        { video: 'Video2', category: 'Category2', userId: 3, comment: 'Issue 3', status: 'waiting' },
      ]);
    });
  });
});
